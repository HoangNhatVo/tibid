const formatMoney = (money) => {
  return parseInt(money).toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
}
const formatMoney2 = (money) => {
  return money.toLocaleString();
}
export default {
  formatMoney,
  formatMoney2
}
