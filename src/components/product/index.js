Component({
  data: {
    milisecond: 1000000000,
    time: 123,
    timer: 0,
  },
  props: {
    items: ''
  },
  onInit() {
    this.setData({
      milisecond: this.props.items.time
    })
  },
  didMount() {
    console.log('vao countdown')
    this.startCountDown();
  },
  methods: {
    milisecondToTime(milisecond) {
      let hours = Math.floor((milisecond) / (1000 * 60 * 60));
      let minutes = Math.floor((milisecond % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((milisecond % (1000 * 60)) / 1000);
      let obj = {
        "h": hours,
        "m": minutes,
        "s": seconds
      };
      return obj;
    },
    startCountDown() {
      this.data.timer = setInterval(() => {
        this.countDown()
      }, 1000);
    },
    countDown() {
      let seconds = this.data.milisecond - new Date().getTime();
      this.setData({
        time: JSON.stringify(this.milisecondToTime(seconds)),
      });
      
      // Check if we're at zero.
      if (seconds == 0) {
        clearInterval(this.timer);
      }
    }
  }
})
