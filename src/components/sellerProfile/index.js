Component ({
  data:{
    products: [],
    test: 0,
    loading: true,
    activeTab: 0,
    isLogged:false,
    seller_id:"162514",
  },
  changeTab(e) {
    this.setData({
      activeTab: parseInt(e.target.targetDataset.index),
      products: []
    })
    const index = e.target.targetDataset.index;
    
    if(index == 0) {
      this.loadPlayingBid(2)
    } else if (index == 1) {
      this.loadPlayingBid(3)
    } else {
      this.loadPlayingBid(1)
    }
  },
  tabItem() {
    my.navigateTo({
      url: `pages/productBidHistory/index`
    })
  },
  // --------- LOAD ---------
  onLoad() {    
    // this.callFakedata();
    this.loadPlayingBid(2)
  },

  loadPlayingBid(status) {
    console.log("load bidding...")
    my.request({
      headers: {
        "content-type": "application/json"
      },
      method: "post",
      url: "https://tibid-22bzhkbi5a-as.a.run.app/orders/search",
      data: {
        "searchCriteria": {
          "productName": "",
          "orderStatus": status,
          "sellerId": this.data.seller_id
        },
        "itemPerPage": "20",
        "pageNum": "0"
      },
      success: (response) => {
        console.log(response.content);
        let items = []
        response.content.forEach(element => {
          element.tikiInfo = JSON.parse(element.tikiInfo);
          items.push(element)
        });

        this.setData({products: items, loading:false});
        my.hideLoading();
      },
      fail : (err) => {
        this.setData({loading:false});
        my.hideLoading();
        console.log("Loi");
        console.log(err);
      }
    })
  },

  onTapUpProduct() {
    my.navigateTo({url:"pages/listSellerProducts/index"});
  }
});
