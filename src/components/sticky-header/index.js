//-------- STICKY headers -----
// before using, set page index.json to have 
// "transparentTitle": "always",
// "defaultTitle":""


Component({
  data: {
    
  },
  props:{    
    onSearch: (data) => console.log(data)
  },
  didMount(){

  },

  methods: {
    onTapSideIcon() {
      console.log('onTapSideIcon');
    },

    onSearch(e){
      console.log(e);
      this.props.onSearch(e.detail.value);
    }
  }
  
});