function formatMoney(money) {
  if(money) {
    return parseInt(money).toLocaleString()+'đ';
  }
  return '';
}

export default {
  formatMoney
}