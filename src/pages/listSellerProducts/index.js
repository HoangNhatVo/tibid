Page ({
  data:{
    products: [],
    loading: true,
    seller_id: "162514",
  },

  onLoad() {
    console.log("list products on load");

    my.getUserInfo({
      success: (res) => {
        console.log(res);
        this.setData({
          userInfo: res
        })
        if (res.customerId == 7766328) {
           this.setData({seller_id: "19152"})
        }
        this.loadProducts();         
      },
      fail: (err) => {
        console.log(err);
        this.loadProducts();
      }
    });

  },

  loadProducts() {
    my.showLoading();
    my.request({
      url: `https://api.tiki.vn/seller-store/v2/collections/6/products?seller_id=${this.data.seller_id}&limit=20&cursor=0`,
      headers: {
        "x-access-token": "Z4evdG2qbNAUkIHiBVfwt3jloPK69MQx",
        "Accept": "application/json",
        "x-source": "local",
        "User-Agent": "duma ocho"
      },
      success: (response) => {
        my.hideLoading();
        console.log(response);
        this.setData({products: response.data, loading:false});
      },
      fail : (err) => {
        my.hideLoading();
        console.log("Looi");
        console.log(err);
      }
    });  
  },

  onPageScroll(event) {
    this.setData({fixedHeader: event.scrollTop > 20});
    my.hideBackHome({ hide: (event.scrollTop > 20) });
  },

  onSearchText(txt){
    console.log(txt);
    const url = `https://api.tiki.vn/seller-store/v2/collections/6/products?seller_id=${this.data.seller_id}&limit=16&cursor=0&keyword=${txt}`;
    console.log(url)
    my.showLoading();
    my.request({
      url: url,
      headers: {
        "x-access-token": "Z4evdG2qbNAUkIHiBVfwt3jloPK69MQx",
        "Accept": "application/json",
        "x-source": "local",
        "User-Agent": "Firefox"
      },
      success: (response) => {
        my.hideLoading();
        console.log(response);
        this.setData({products: response.data, loading:false});
      },
      fail : (err) => {
        my.hideLoading();
        console.log("Looi");
        console.log(err);
      }
    });
  },

  // Navigate
  onTabProduct(e) {
    console.log(e)
    const item = e.target.dataset.item;
    const productStr = JSON.stringify(item);
    my.navigateTo({
      url: `pages/createBidProduct/inputScreen/index?sellerId=${this.data.seller_id}&tikiInfo=${productStr}`
    });
  }

});