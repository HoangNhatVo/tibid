Page ({
  data:{
    isLogged:false,
  },
  onLoad() {
  },

  onShow() {
    console.log("onShow");
    this.tryGetUserInfo()
  },
  onReady() {

  },

  tryGetUserInfo() {
    console.log("try get user info");
    my.showLoading();
    my.getUserInfo({
      success: (res) => {
        my.hideLoading();
        console.log(res);
        this.setData({userInfo:res, isLogged:true});
        if (res.customerId == 7766328) {
          my.navigateTo({ url: "pages/profile/seller/index"});
        } else {
          my.navigateTo({ url: "pages/profile/index"});
        }
      },
      fail: (err) => {
        my.hideLoading();
        console.log(err);
        this.setData({isLogged:false});
      }
    });
  },
});
