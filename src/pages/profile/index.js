Page ({
  data:{
    products: [],
    loading: true,
    activeTab: 0,
    userInfo: {},
    typeUser: '',
    seller_id: "162514"
  },
  tabItem(e) {
    const id = e.target.dataset.index;
    my.navigateTo({
      url: `pages/productBidHistory/index?id=${id}`
    })
  },
  onLoad() {
    
  },

  onShow(){
    my.showLoading();
    my.getUserInfo({
      success: (res) => {
        console.log(res);
        my.hideLoading()
        this.setData({
          userInfo: res
        })
        
        if (res.customerId == 7766328) {
           this.setData({seller_id: "19152"})
        }

        if (res.customerId == 7766328 || res.customerId == 12218390) {
          this.setData({
            typeUser: 'shop'
          })
          this.loadPlayingBid(2)
        } else {
          this.setData({
            typeUser: 'user'
          })
          this.getData(res.customerId, this.data.activeTab);          
        }
      },
      fail: (err) => {
        console.log(err);
        my.hideLoading();
      }
    });
  },

  getData(customerId, status) {
    my.request({
      url: `https://tibid-22bzhkbi5a-as.a.run.app/tickets/search?userId=${customerId}&status=${status+1}`,
      success: (response) => {
        let data = [];
        if(response.length > 0) {
            response.forEach(element => {
            element.orderDto.tikiInfo = JSON.parse(element.orderDto.tikiInfo);
            data.push(element)
          });
        }
        this.setData({
          products: data
        })
      },
      fail : (err) => {
        console.log(err)
                my.alert({
          title: JSON.stringify(err)
        })
      }
  })
  },
  changeTab(e) {
    const tab = e.target.targetDataset.index;
    this.getData(this.data.userInfo.customerId, tab);
    this.setData({
      activeTab: parseInt(tab),
    })
  },
  //====================shop======
  changeTabShop(e) {
    this.setData({
      activeTab: parseInt(e.target.targetDataset.index),
      products: []
    })
    const index = e.target.targetDataset.index;
    
    if(index == 0) {
      this.loadPlayingBid(2)
    } else if (index == 1) {
      this.loadPlayingBid(3)
    } else {
      this.loadPlayingBid(1)
    }
  },

  loadPlayingBid(status) {
    my.request({
      headers: {
        "content-type": "application/json"
      },
      method: "post",
      url: "https://tibid-22bzhkbi5a-as.a.run.app/orders/search",
      data: {
        "searchCriteria": {
          "productName": "",
          "orderStatus": status,
          "sellerId": this.data.seller_id
        },
        "itemPerPage": "20",
        "pageNum": "0"
      },
      success: (response) => {
        let items = []
        response.content.forEach(element => {
          element.tikiInfo = JSON.parse(element.tikiInfo);
          items.push(element)
        });

        this.setData({products: items, loading:false});
        my.hideLoading();
      },
      fail : (err) => {
        this.setData({loading:false});
        my.hideLoading();
        console.log("Loi");
        console.log(err);
      }
    })
  },
  onTapUpProduct() {
    my.navigateTo({url:"pages/listSellerProducts/index"});
  }
});
