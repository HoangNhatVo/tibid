Page({
  data: {
    products: [],
    test:{}
  },
  parseJson(item) {
    return JSON.parse(item);
  },
  onLoad(query) {
    this.loadData();
  },
  loadData() {
    my.request({
      url: 'https://tibid-22bzhkbi5a-as.a.run.app/orders/search',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        "searchCriteria": {
        "productName":""
      },
        "itemPerPage":"20",
        "pageNum":"0"
      },
      success: (response) => {
        let data = []
        response.content.forEach(element => {
          element.tikiInfo = JSON.parse(element.tikiInfo);
          data.push(element)
        });
        this.setData({products: data, loading:false});
        my.hideLoading();
        my.stopPullDownRefresh();
      },
      fail : (err) => {
        my.hideLoading();
        console.log("Looi");
        console.log(err);
        my.stopPullDownRefresh();
      }
    });  
  },

  onReady() {
  },
  onShow() {
  },
  onHide() {
  },
  onUnload() {
  },
  onTab(e) {
    const item = e.target.dataset.item;
    my.navigateTo({
      url: `pages/productBidHistory/index?id=${item}`
    })
  },

  // Pull 2 request 
  onPullDownRefresh() {
    //do load data here
    console.log('onPullDownRefresh is triggered');
    this.loadData();
  },
  onStartPullDown() {
    my.startPullDownRefresh();
  },
  onStopPullDown() {
    my.stopPullDownRefresh();
  }
});