import query from 'query-string';
import SockJS from "sockjs-client";
var Stomp = require("@stomp/stompjs");

Page({
  data: {
    isShowDescription: false,
    modal: {},
    modalbuy: {},
    data: {},
    userInfo: {},
    error: {},
    currentPrice: 250000,
    inputmoney: '',
    bidsuccess: false,
    historyBid: [],
    time: {},
    timer: 0,
    id: "",
    socketConnected: false
  },
  
  connectStompSocket(id) {
    if (this.data.socketConnected) {
      return
    }
    var stompClient = null;
    var socket = new SockJS('https://tibid-22bzhkbi5a-as.a.run.app/ws');
          stompClient = Stomp.Stomp.over(socket);  
          const page = this;
          stompClient.connect({}, function(frame) {
              page.setData({socketConnected:true});
              // console.log('Connected: ' + frame);
              stompClient.subscribe('/topic/order/'+id, function(messageOutput) {
                  const body = JSON.parse(messageOutput.body);
                  console.log(body);
                  // TODO: update history
                  page.onNewBidComming(body.lastTicket);
              });
          });
  },

  onNewBidComming(ticket) {
    console.log(ticket);
    const data = [...this.data.historyBid];
    const time = this.formatDate(ticket.createdAt)

    var nticket = ticket;
    nticket.createdAt = time

    data.unshift(nticket);
    this.setData({
      historyBid: data,
      currentPrice: ticket.price
    })
    console.log(this.data.historyBid);
  },

  onLoad(options) {
    const id = query.parse(options).id;
    console.log(id);
    this.setData({id : id}); 
    my.getUserInfo({
      success: (res) => {
        this.data.userInfo=res;
      }
    });
    
    this.loadData(id);
  },

  loadData(id) {
    console.log("loadData");
    my.showLoading();
    my.request({
      url: `https://tibid-22bzhkbi5a-as.a.run.app/orders/${id}`,
      method: 'GET',
      success: (response) => {
        console.log(response);
        response.bidOrder.tikiInfo = JSON.parse(response.bidOrder.tikiInfo);
        let history = []
        response.bidHistory.forEach(element => {
          element.createdAt = this.formatDate(element.createdAt)
          history.push(element)
        });
        this.setData({
          data: response.bidOrder,
          historyBid: history
        })
        my.hideLoading();
        this.connectStompSocket(id);
        this.startCountDown();
      },
      fail : (err) => {
        this.setData({
          error: err
        })
        my.hideLoading();
        console.log("Looi");
        console.log(err);
      }
    });
  },

  onReady() {
    //tao connect websocket

  },
  inputMoney(e) {
    if(e.detail.value.includes(',')){
      this.setData({
        inputmoney: parseInt(e.detail.value.split(",").join(""))
    })
    } else {
      this.setData({
        inputmoney: parseInt(e.detail.value.split(".").join(""))
      })
    }
  },
  buyNow(){
    my.showLoading();
    console.log(this.data);
    my.request({
      url: `https://tibid-22bzhkbi5a-as.a.run.app/orders/${this.data.data.id}/bidWin`,
      method: 'POST',
      headers:{
        "Content-Type": "application/json"
      },
      data: {
        "userId": this.data.userInfo.customerId,
        "userName": this.data.userInfo.name,
        "userAvatar": this.data.userInfo.avatar,
        "price": this.data.data.ceilingPrice
      },
      success: (response) => {
        my.hideLoading();
        console.log(response);
      },
      fail : (err) => {
        my.hideLoading();
        console.log("Looi");
        console.log(err);
      }
    });
  },

  onBidFake() {
    const data = [...this.data.historyBid];
    data.unshift({
      url: 'https://scontent-hkg4-2.xx.fbcdn.net/v/t1.6435-1/p200x200/162431546_2965684776984748_111116095801194960_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=7206a8&_nc_ohc=PXudgv5GawYAX-6Hni8&_nc_ht=scontent-hkg4-2.xx&oh=d1141db8dcf26c207f89e808257c02c6&oe=61200BFD',
      name: 'Hoàng nhật',
      price: '300.000',
      time: '10:43:54 22/07/2021'
    })
    this.setData({
      historyBid: data,
      currentPrice: 300000
    })
    this.setData({
      bidsuccess: true
    })
  },

  onBid() {
    my.showLoading();
    console.log(this.data);
    my.request({
      url: `https://tibid-22bzhkbi5a-as.a.run.app/orders/${this.data.data.id}/bid`,
      method: 'POST',
      headers:{
        "Content-Type": "application/json"
      },
      data: {
        "userId": this.data.userInfo.customerId,
        "userName": this.data.userInfo.name,
        "userAvatar": this.data.userInfo.avatar,
        "price": this.data.inputmoney
      },
      success: (response) => {
        my.hideLoading();
        console.log(response);
      },
      fail : (err) => {
        my.hideLoading();
        console.log("Looi");
        console.log(err);
      }
    });
    this.closeModal()
  },

  changeTab() {
    this.setData({
      isShowDescription: !this.data.isShowDescription
    })
  },
  closeModal() {
    this.setData({
      bidsuccess: false,
      modal: {},
      modalbuy: {}
    });
  },
  closeModalBuy() {
    this.setData({
      modalbuy: {}
    })
  },
  showModal(e) {
    this.setData(e.target.dataset);
  },
  milisecondToTime(milisecond) {
    let hours = Math.floor((milisecond) / (1000 * 60 * 60));
    let minutes = Math.floor((milisecond % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((milisecond % (1000 * 60)) / 1000);

    let obj = {
      "h": hours.toString().length <2 ? `0${hours}`: hours,
      "m": minutes.toString().length <2 ? `0${minutes}`: minutes,
      "s": seconds.toString().length <2 ? `0${seconds}`: seconds
    };
    return obj;
  },
  startCountDown() {
    this.data.timer = setInterval(() => {
      this.countDown()
    }, 1000);
  },
  countDown() {
    let seconds = this.data.data.bidEndTime - new Date().getTime();
    if(seconds > 0) {
        this.setData({
          time: this.milisecondToTime(seconds),
      });
    } else {
        this.setData({
          time: {
            "h": '00',
            "m": '00',
            "s": '00',
          },
        })
      }
    
    // Check if we're at zero.
    if (seconds <= 0) {
      clearInterval(this.data.timer);
    }
  },
  formatDate(milisecond) {
    const day = new Date(milisecond).getDate();
    const month = new Date(milisecond).getMonth() +1;
    const year = new Date(milisecond).getUTCFullYear();
    const hour = new Date(milisecond).getHours();
    const minute = new Date(milisecond).getMinutes();
    const second = new Date(milisecond).getSeconds();
    return `${day}/${month}/${year} ${hour}:${minute}:${second}`;
  }
});