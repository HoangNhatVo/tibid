import query from 'query-string';

Page({
  data: {    
    product: {
      name: "Xiaomi 19.5 pro max",
      minPrice: 2000,
      price: 11200000,
      thumbnail_url: "https://salt.tikicdn.com/cache/280x280/ts/product/34/39/cd/6bc5730e865d2fe22efa4eff0a4fcc8b.jpg",
      stock_item: {
        qty: 127                
      },
    },
    sellerId:"162514",
    tikiInfo: "",

    hours: Array.from(Array(24).keys()),
    minutes: Array.from(Array(60).keys()),
    days: Array.from({length: 31}, (_, i) => i + 1),
    months: Array.from({length: 12}, (_, i) => i + 1),

    qty: 1,
    startDate: new Date(),
    endDate: new Date(),
    startPrice: 1000,
    stepPrice: 1000,
    ceilingPrice:0
  },

  onShow(){

  },

// Start time ----
  onHourChange(event) {
    console.log("change hour");
    this.setData({hIndex: event.detail.value});
  },
  onMinuteChange(event) {
    console.log("change hour");
    this.setData({mIndex: event.detail.value});
  },
  onDateChange(event) {
    console.log("change hour");
    this.setData({selectDate: event.detail.value});
  },
  onMonthChange(event) {
    console.log("change hour");
    this.setData({selectMonth: event.detail.value});
  },
// End time --------
  onHourChangeE(event) {
    console.log("change hour");
    this.setData({hIndexE: event.detail.value});
    endDate.setHours(hIndexE);
  },
  onMinuteChangeE(event) {
    console.log("change hour");
    this.setData({mIndexE: event.detail.value});
  },
  onDateChangeE(event) {
    console.log("change hour");
    this.setData({selectDateE: event.detail.value});
  },
  onMonthChangeE(event) {
    console.log("change hour");
    this.setData({selectMonthE: event.detail.value});
  },

  onQtyChange(value) {
    console.log(value);
  },

  onMaxPriceChange(e) {
    console.log(e);
    this.setData({ceilingPrice: e.detail.value});
  },

  onStartPriceChange(e) {
    console.log(e);
    this.setData({startPrice: e.detail.value});
  },

  onStepPriceChange(e) {
    this.setData({stepPrice: e.detail.value});
  },

  removeMoneyFormat(value) {
    // if(value.includes(',')){
    //   return parseInt(value.split(",").join("")
    // })
    // } else {
    //   return parseInt(value.split(".").join(""))
    // }
  },

  onLoad(options){    
    if (options.length > 0) {
      const data = query.parse(options);
      console.log(data);  // data: {seller_id, tikiInfo}
      const product = JSON.parse(data.tikiInfo);
      this.setData({
        tikiInfo: data.tikiInfo,
        product: product,
        sellerId: data.sellerId
      });
      this.setData({
        ceilingPrice: this.data.product.price
      });
    }
    
    const now = new Date();
    console.log("On Load");
    this.setData({
      selectDate: now.getDate()-1,
      selectMonth: now.getMonth()-1,
      hIndex: now.getHours(),
      mIndex: now.getMinutes()+1,
      selectDateE: now.getDate()-1,
      selectMonthE: now.getMonth()-1,
      hIndexE: now.getHours(),
      mIndexE: now.getMinutes()-1,
    });
  },

  onTouchPostProduct() {
    console.log("Up product!");
    var sDate = new Date();
    sDate.setHours(this.data.hIndex);
    
    sDate.setMinutes(this.data.mIndex);
    sDate.setDate(this.data.selectDate+1);
    sDate.setMonth(this.data.selectMonth+1);
    const bidStartTime = sDate.getTime();
    // console.log(bidStartTime);

    var eDate = new Date()
    eDate.setHours(this.data.hIndexE);
    eDate.setMinutes(this.data.mIndexE);
    eDate.setDate(this.data.selectDateE+1);
    eDate.setMonth(this.data.selectMonthE+1);
    const bidEndTime = eDate.getTime();
    console.log(bidEndTime);

    const data = {
        "userId": this.data.sellerId,
        "productId": this.data.product.id,
        "startPrice": this.data.startPrice,
        "priceStep":this.data.stepPrice,
        "status":"1",
        "bidStartTime":bidStartTime,
        "bidEndTime":bidEndTime,
        "bidQuantity": 1,
        "productName":this.data.product.name,
        "ceilingPrice": this.data.ceilingPrice,
        "tikiInfo": this.data.tikiInfo
    };
    console.log("data:");
    console.log(data);

    my.showLoading();
    my.request({
      url: "https://tibid-22bzhkbi5a-as.a.run.app/orders",
      method: "POST",
      headers:{
        "Content-Type": "application/json"
      },
      data: data,
      success: (res) => {
        my.hideLoading();
        console.log("Success");
        console.log(res);
        this.showToast(true);

      },
      fail: (err) => {
        console.log("errrorr");
        my.hideLoading();
        console.log(err);
        this.showToast(false);
      }
    });
  },

  showToast(isSuccess) {
    my.showToast({
      type: isSuccess? "success":"fail",
      content: isSuccess? "Đăng sản phẩm đấu giá thành công":"Có lỗi xảy ra, hãy thử lại sau",
      buttonText: "OK",
      duration: 3000,
      success: () => {
        // my.alert({ title: "success" });
      },
      fail: (e) => {
        // my.alert({ title: `${e}` });
      },
      complete: () => {
        console.log("Complete");
      },
    });
  }
  

});
